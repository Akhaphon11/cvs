import tkinter as tk
from tkinter import ttk
from functools import partial
import csv
# from Login import AppLogin

# from home_tk import HomeFrame

# from login import Login

from login_tk import Login
from register_tk import Register

# Pre-define some defaults
FONT_LARGE = ("Lato", 10)
FONT_HEAD = ("Lato", 20)
    

class IndexFrame(tk.Frame):
    def __init__(self, parent, controller, session_name):

        tk.Frame.__init__(self, parent, bg='CadetBlue2')

        img = Image.open(os.path.join(os.getcwd(), 'static',
                                      'pic', 'brainstorming256.png'))
        photo = ImageTk.PhotoImage(img)
        label = tk.Label(self, image=photo, bg='CadetBlue2')
        label.image = photo
        # label_icon.pack(pady = 20)
        # label.pack(side = 'left',pady = 20, padx = 40, anchor = 'nw')
        label.place(x=350, y=10)

        self.sesion_name = "Index_session_name"

        # self.parent = self
        self.controller = controller

        # Create a text label and place it in the window
        self.hello_label = tk.Label(
            self, text="APPLICATION FOR EYE FATIGUE NOTIFICATION IN CASE OF", font=FONT_HEAD, bg='CadetBlue2')
        self.hello_label.place(x=145, y=275, bordermode=tk.OUTSIDE)
        # self.hello_label.pack(pady = 100, anchor = 'center')

        self.hello_label = tk.Label(
            self, text="PROLONGED USE OF COMPUTER USING IMAGE FROM WEB CAMERA", font=FONT_HEAD, bg='CadetBlue2')
        self.hello_label.place(x=80, y=345)
        # self.hello_label.pack(anchor = 'center1')

        #ฺBottonRegister
        self.Register_button = tk.Button(
            self, text="Register", font=FONT_LARGE, bg='lemon chiffon', height=2, width=7, command=Register)
        self.Register_button.place(x=550, y=470)
        # self.Register_button.pack(pady = 50, side = 'bottom',anchor = 'center')

        #ฺBottonLogin
        self.Login_button = tk.Button(
            self, text="Login", font=FONT_LARGE, bg='lemon chiffon', height=2, width=8, command=self.login_screen)

        # self.Login_button.pack(side = 'bottom',pady = 20,anchor = 'center')
        self.Login_button.place(x=350, y=470)

    def login_screen(self):
        font_ = ('Lato', 12)
        self.login_screen = tk.Toplevel(self, bg='LightSkyBlue1')
        self.login_screen.title("Login")
        self.login_screen.geometry('370x350')

        # img = Image.open(os.path.join(os.getcwd(), 'static', 'pic', 'red.jpg'))
        # photo = ImageTk.PhotoImage(img)
        # bg_label = tk.Label(self.login_screen, image = photo)
        # bg_label.image = photo
        # bg_label.place(x=0, y=0, relwidth=1, relheight=1)

        # canvas = tk.Canvas(self.login_screen)
        # canvas.pack()
        # img = tk.PhotoImage(file = 'login.png')
        # canvas.create_image(image = img)

        img_icon = Image.open(os.path.join(
            os.getcwd(), 'static', 'icon', 'login64.png'))
        photo_icon = ImageTk.PhotoImage(img_icon)
        label_icon = tk.Label(
            self.login_screen, image=photo_icon, bg='LightSkyBlue1')
        label_icon.image = photo_icon
        label_icon.pack(pady=20)
        # label_icon.place(x=10,y = 10)

        self.firstNameLabel = tk.Label(
            self.login_screen, font=font_, text="User name", bg='LightSkyBlue1').pack()
        self.firstName = tk.StringVar()
        # self.firstNameEntry = tk.Entry(self.login_screen, font = font_, textvariable=self.firstName).place(x = 150, y = 100)
        self.firstNameEntry = tk.Entry(
            self.login_screen, font=font_, textvariable=self.firstName, justify='center').pack()

        self.passwdLabel = tk.Label(
            self.login_screen, text="Password", font=font_, bg='LightSkyBlue1').pack()
        self.passwd = tk.StringVar()
        # self.passwdEntry = tk.Entry(self.login_screen, textvariable=self.passwd,font = font_, show = "*").place(x = 350, y = 200)
        self.passwdEntry = tk.Entry(
            self.login_screen, textvariable=self.passwd, font=font_, show="*", justify='center').pack()

        #login button
        self.loginButton = tk.Button(self.login_screen, text="Login", font=font_,
                                     bg='lemon chiffon', command=self.login_verify).place(x=120, y=250)
        # self.loginButton = tk.Button(self.login_screen, text="Login", font=font_, command=self.login_verify).pack()

        self.btn_cancle = tk.Button(self.login_screen, text='Cancel', font=font_, bg='lemon chiffon',
                                    command=lambda: self.login_screen.destroy()).place(x=200, y=250)
        # self.btn_cancle = tk.Button(self.login_screen, text = 'Cancel',font = font_, command = lambda: self.login_screen.destroy()).pack()

    def login_verify(self):
        userName = self.firstName.get()
        passwd = self.passwd.get()
        prefix = 'data/'
        postfix = '_info.csv'

        list_files = os.listdir('data/')
        if userName in list_files:
            pass_user = ""
            with open(prefix + userName + "/" + userName + postfix, 'r', newline='') as csvfile:
                    reader = csv.DictReader(csvfile)
                    # print("Hello : " + reader['userName'])
                    for row in reader:
                            # print('Hello :'+row['userName'])
                            # print('login none')
                            pass_user = row['password']
                    if passwd == pass_user:
                        # print('login successs')
                        # global user_session
                        user_session = userName
                        # HomeFrame.label_name.config(text = user_session)
                        # print(user_session)
                        self.login_success()
                    else:
                        # print('password not correct.')
                        self.password_not_recognised()
        else:
            # print('No user.')
            self.user_not_found()

    def login_success(self):
        self.login_success_screen = tk.Toplevel(
            self.login_screen, bg='LightSkyBlue1')
        self.login_success_screen.geometry('100x100')

        self.login_success_screen.title('Success')
        self.login_success_screen.geometry('150x100')
        label_success = tk.Label(
            self.login_success_screen, text="Login Success", bg='LightSkyBlue1', font=('Lato', 12))
        label_success.pack(side='top', pady = 15)
        btn_success = tk.Button(self.login_success_screen, text='Ok', font=(
            'Lato', 12), bg='lemon chiffon', command=self.delete_login_success)
        btn_success.pack(side='top', pady = 5)

    def delete_login_success(self):
        self.login_success_screen.destroy()
        self.controller.show_frame(HomeFrame)
        self.controller.set_session_name(self.firstName)
        self.login_screen.destroy()
        self.controller.set_session_name(self.firstName.get())

    def password_not_recognised(self):
        self.password_not_recog_screen = tk.Toplevel(
            self.login_screen, bg='LightSkyBlue1')
        self.password_not_recog_screen.title('Success')
        self.password_not_recog_screen.geometry('150x100')
        label_success = tk.Label(self.password_not_recog_screen, text="Invalid Password", bg='LightSkyBlue1')
        label_success.pack()
        btn_pass_not_recog = tk.Button(self.password_not_recog_screen, text=" Ok",
                                       bg='lemon chiffon', command=self.delete_password_not_recognise)
        btn_pass_not_recog.pack()

    def delete_password_not_recognise(self):
        self.password_not_recog_screen.destroy()

    def user_not_found(self):
        self.user_not_found_screen = tk.Toplevel(
            self.login_screen, bg='LightSkyBlue1')
        self.user_not_found_screen.title('Success')
        self.user_not_found_screen.geometry('150x100')
        label_not_found = tk.Label(
            self.user_not_found_screen, text="user not found.", bg='LightSkyBlue1')
        label_not_found.pack()
        btn_not_found = tk.Button(self.user_not_found_screen, text="Ok",
                                  bg='lemon chiffon', command=self.delete_user_not_found_screen)
        btn_not_found.pack()

    def delete_user_not_found_screen(self):
        self.user_not_found_screen.destroy()

    def get(self):
        return self.firstName


# if __name__ == "__main__":
    # root = tk.Tk()
    # app = AppWindow(root, "test_app", "1000x700")
    # root = tkApp()
    # root.mainloop()


