# from tkinter import * as tk
import threading
import cv2
import dlib
import numpy as np
from imutils import face_utils
from imutils.video import FileVideoStream
from imutils.video import VideoStream
from scipy.spatial import distance as dist
import imutils
import os
import sys

from src.myVideoCapture import MyVideoCapture

class Detect(MyVideoCapture):
    def __init__(self,  video_source = 0):
        sys.path.append(os.getcwd())
        self.video_source = video_source
        super().__init__(self.video_source)
        # self.video_source = video_source
        # self.vid = MyVideoCapture(video_source)
        # self.fileStream = True
        self.detector = self.getDetector()
        # self.predictor = self.getPredictorFile()
        self.predictor = dlib.shape_predictor(os.path.join(os.getcwd(),"src","shape_predictor_68_face_landmarks.dat"))
        self.ear = 0.0
        self.TOTAL = 0
        

        # self.width = self.vid.get(cv2.CAP_PROP_FRAME_WIDTH)
        # self.height = self.vid.get(cv2.CAP_PROP_FRAME_HEIGHT)

    def eye_aspect_ratio(self, eye):
        # compute the euclidean distances between the two sets of
        # vertical eye landmarks (x, y)-coordinates
        A = dist.euclidean(eye[1], eye[5])
        B = dist.euclidean(eye[2], eye[4])

        # compute the euclidean distance between the horizontal
        # eye landmark (x, y)-coordinates
        C = dist.euclidean(eye[0], eye[3])

        # compute the eye aspect ratio
        ear = (A + B) / (2.0 * C)

        # return the eye aspect ratio
        return ear

    # overide methods
    def get_frame(self):
        if self.vid.isOpened():
            ret, frame = self.vid.read()
            if ret:
                return (ret, cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
            else:
                # self.vid.release()
                return (ret, None)
        else:
            self.vid.release()
            return (ret, None)

    def get_frame_detect(self):
        # if (self.fileStream and not self.vd.more()):
        #     self.vd.stop()
        # frame = self.vd.read()
        # frame = imutils.resize(frame, width=450)
        ret, frame = self.get_frame()

        (lStart, lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
        (rStart, rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]

        gray = cv2.cvtColor(frame,  cv2.COLOR_BGR2GRAY)

        rects = self.detector(gray,)

        # if not rects:
        #     print('Not rect')

        for rect in rects:
            shape = self.predictor(gray, rect)
            shape = face_utils.shape_to_np(shape)

            leftEye = shape[lStart:lEnd]
            rightEye = shape[rStart:rEnd]
            leftEAR = self.eye_aspect_ratio(leftEye)
            rightEAR = self.eye_aspect_ratio(rightEye)

            self.ear = (leftEAR + rightEAR) / 2.0  # float of ear
            # self.ear = ear
            leftEyeHull = cv2.convexHull(leftEye)
            rightEyeHull = cv2.convexHull(rightEye)
            cv2.drawContours(frame, [leftEyeHull], -1, (0, 255, 0), 1)
            cv2.drawContours(frame, [rightEyeHull], -1, (0, 255, 0), 1)

            # cv2.putText(frame, "Blinks: {}".format(self.TOTAL), (10, 30),
            #             cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)

            cv2.putText(frame, "EAR: {:.2f}".format(self.ear), (300, 30),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
            # cv2.putText(frame, "EAR: 0.19".format(self.ear), (300, 30),
            #             cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
        
        return (ret, frame)

    # override methods
    def stop_video(self):
        # if self.vid.isOpened():
            # self.vid.release()
        super().stop_video()


    #get detectorfile
    def getDetector(self):
        try:
            detector = dlib.get_frontal_face_detector()
        except Exception as e:
            return e
        return detector

    def getPredictorFile(self):
        try:
            predictor = dlib.shape_predictor(os.path.join(os.getcwd(),"cvs","shape_predictor_68_face_landmarks.dat"))
            # predictor = dlib.shape_predictor(r'shape_predictor_68_face_landmarks.dat')
            
        except Exception as e:
            return e
        return predictor

    def getTOTAL(self, ear):
        if ear < self.EYE_AR_THRESH:
            self.COUNTER += 1
        else:
            if self.COUNTER >= self.EYE_AR_CONSEC_FRAMES:
                self.TOTAL += 1
            self.COUNTER = 0

    def __del__(self):
        if self.vid.isOpened():
            self.vid.release()

        
