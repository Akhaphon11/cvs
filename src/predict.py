import pickle
import os
from pathlib import Path
import csv


class Predict:
    def __init__(self, ear, blink):
        self.ear = ear
        self.blink = blink
        model_name = self.selectModel()
        # self.selectModel()
        self.model = self.loadModel(model = model_name)
        # print(f'Model in constructor is :: {model_name}\n')

    def predictor(self):
        model = self.model
        pred = model.predict([[(self.ear), (self.blink)]])
        return pred

    def loadModel(self, model = "CVS_Model.sav"):
        prefix = './static/'
        
        # path = 'static/file'
        # file = "C:\\Users\\AHoys\\flask_detection\\src\\CVS_Model.sav"
        # filename = "C:\\Users\\AHoys\\p-project\\new_frame\\CVS_Model.sav"
        load_model = pickle.load(open(prefix +  model, 'rb'))
        # load_model = pickle.load( open(os.path.join(os.getcwd(), 'flask_detection\\static', 'files\\CVS_Model.sav'), 'rb'))
        # load_model = pickle.load(open(os.path.join(os.getcwd(),"cvs","static","CVS_Model.sav"), 'rb'))
        return load_model

    def selectModel(self):
        model = ''
        with open('./data/setting.csv', 'r', newline ='') as file:
            reader = csv.DictReader(file)
            for row in reader:
                model  = row['model']

            file.close()

        return model


# Test load model
# p = Predict(0.23, 15)
# print(p.loadModel())
