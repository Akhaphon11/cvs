import tkinter as tk
import csv
import os
from PIL import ImageTk, Image



class Edit_user(tk.Toplevel):

    def __init__(self, user_name):
        tk.Toplevel.__init__(self, bg='ghost white')
        # self.geometry('300x600')
        self.title('Edit user')

        font_ = ("Lato", 12)
        font_value = ('Lato', 12)

        self.user_name = user_name

        # self.UserNameLabel = tk.Label(self, text="User Name", font = font_).grid(row=0, column=0)
        # self.UserName = tk.StringVar()
        # self.UserNameEntry = tk.Entry(self, textvariable=self.UserName).grid(row=0, column=1) 

        # #FristName label and text entry box
        # self.FristNameLabel = tk.Label(self, text="First Name", font = font_).grid(row=1, column=0)
        # self.FristName = tk.StringVar()
        # self.FristNameEntry = tk.Entry(self, textvariable=self.FristName).grid(row=1, column=1)

        # self.UserNameLabel = tk.Label(self, text="User Name", font = font_).grid(row=0, column=0)
        # self.UserName = tk.StringVar()
        # self.UserNameEntry = tk.Entry(self, textvariable=self.UserName).grid(row=0, column=1) 

        # #FristName label and text entry box
        # self.FristNameLabel = tk.Label(self, text="First Name", font = font_).grid(row=1, column=0)
        # self.FristName = tk.StringVar()
        # self.FristNameEntry = tk.Entry(self, textvariable=self.FristName).grid(row=1, column=1)  

        # #LastName label and LastName entry box
        # self.LastNameLabel = tk.Label(self,text="Last Name", font = font_).grid(row=2, column=0)  
        # self.LastName = tk.StringVar()
        # self.LastNameEntry = tk.Entry(self, textvariable=self.LastName).grid(row=2, column=1)  

        # #Age label and Age entry box
        # self.AgeLabel = tk.Label(self,text='Age', font = font_).grid(row=3, column=0)  
        # self.Age = tk.StringVar()
        # self.AgeEntry = tk.Entry(self, textvariable=self.Age).grid(row=3, column=1)  

        # #TimeSleep label and TimeSleep entry box
        # self.TimeSleepLabel = tk.Label(self,text="Bed time", font = font_).grid(row=4, column=0)  
        # self.TimeSleep = tk.StringVar()
        # self.TimeSleepEntry = tk.Entry(self, textvariable=self.TimeSleep).grid(row=4, column=1)  

        # # Job
        # self.JobLabl = tk.Label(self, text="Job", font = font_).grid(row= 8, column = 0)
        # self.Job = tk.StringVar()
        # self.JobEntry = tk.Entry(self, textvariable=self.Job).grid(row= 8, column = 1)

        # #TimeCom label and TimeCom entry box
        # self.TimeComLabel = tk.Label(self,text="Computer time", font = font_).grid(row=5, column=0)  
        # self.TimeCom = tk.StringVar()
        # self.TimeComEntry = tk.Entry(self, textvariable=self.TimeCom).grid(row=5, column=1)  

        # #Disease label and Disease entry box
        # self.DiseaseLabel = tk.Label(self,text="Disease", font = font_).grid(row=6, column=0)  
        # self.Disease = tk.StringVar()
        # self.DiseaseEntry = tk.Entry(self, textvariable=self.Disease).grid(row=6, column=1) 

        # #password label and password entry box
        # self.passwordLabel = tk.Label(self,text="Password", font = font_).grid(row=7, column=0)  
        # self.password = tk.StringVar()
        # self.passwordEntry = tk.Entry(self, textvariable=self.password).grid(row=7, column=1)
        
        # self.UserNameLabel = tk.Label(self, text="User Name", font = font_).pack()
        # self.UserName = tk.StringVar()
        # self.UserNameEntry = tk.Entry(self, textvariable=self.UserName, font = font_value).pack()

        # #FristName label and text entry box
        # self.FristNameLabel = tk.Label(self, text="First Name", font = font_).pack()
        # self.FristName = tk.StringVar()
        # self.FristNameEntry = tk.Entry(self, textvariable=self.FristName, font = font_value).pack()

        img_icon = Image.open(os.path.join(os.getcwd(), 'static', 'icon', 'edit128_2.png'))
        photo_icon = ImageTk.PhotoImage(img_icon)
        label_icon = tk.Label(self, image=photo_icon, bg='ghost white')
        label_icon.image = photo_icon
        # label_icon.pack(pady = 20)
        label_icon.grid(row = 0, column = 0, columnspan = 5, padx = 20, pady = 10)
        
        self.UserNameLabel = tk.Label(self, text="User Name : ", font=font_, bg='ghost white').grid(
            row=1, column=0, padx=5, pady=5, sticky='e')
        self.UserName = tk.StringVar()
        self.UserNameEntry = tk.Entry(self, textvariable=self.UserName, font = font_value).grid(row = 1, column =1, columnspan = 2, padx = 5, pady = 5)

        #FristName label and text entry box
        self.FristNameLabel = tk.Label(
            self, text="First Name : ", font=font_, bg='ghost white').grid(row=2, column=0, padx=5, pady=5, sticky='e')
        self.FristName = tk.StringVar()
        self.FristNameEntry = tk.Entry(self, textvariable=self.FristName, font = font_value).grid(row = 2, column =1, columnspan = 2, padx = 5, pady = 5)  

        #LastName label and LastName entry box
        self.LastNameLabel = tk.Label(
            self, text="Last Name : ", font=font_, bg='ghost white').grid(row=3, column=0, padx=5, pady=5, sticky='e')
        self.LastName = tk.StringVar()
        self.LastNameEntry = tk.Entry(self, textvariable=self.LastName, font = font_value).grid(row = 3, column =1 ,columnspan = 2, padx = 5, pady = 5)  

        #Age label and Age entry box
        self.AgeLabel = tk.Label(
            self, text='Age : ', font=font_, bg='ghost white').grid(row=4, column=0, padx=5, pady=5, sticky='e')
        self.Age = tk.StringVar()
        self.AgeEntry = tk.Entry(self, textvariable=self.Age, font = font_value).grid(row = 4, column = 1, columnspan = 2, padx = 5, pady = 5)  
        self.Age_y = tk.Label(self, text='year', font=font_, bg='ghost white').grid(
            row=5, column=4, padx=5, pady=5, sticky = 'w')

        #TimeSleep label and TimeSleep entry box
        self.TimeSleepLabel = tk.Label(
            self, text="Bed time : ", font=font_, bg='ghost white').grid(row=5, column=0, padx=5, pady=5, sticky='e')
        self.TimeSleep = tk.StringVar()
        self.TimeSleepEntry = tk.Entry(self, textvariable=self.TimeSleep, font = font_value).grid(row = 5, column =1, columnspan = 2, padx = 5, pady = 5 )  
        self.TimeSleepHour_lebel = tk.Label(
            self, text='Hour / Day', font=font_, bg='ghost white').grid(row=6, column=4, padx=5, pady=5, sticky='w')

        # Job
        self.JobLabl = tk.Label(
            self, text="Job : ", font=font_, bg='ghost white').grid(row=6, column=0, padx=5, pady=5, sticky='e')
        self.Job = tk.StringVar()
        self.JobEntry = tk.Entry(self, textvariable=self.Job, font = font_value).grid(row = 6, column = 1, columnspan =2, padx = 5, pady = 5)

        #TimeCom label and TimeCom entry box
        self.TimeComLabel = tk.Label(
            self, text="Use Computer time : ", font=font_, bg='ghost white').grid(row=7, column=0, padx=5, pady=5, sticky='e')
        self.TimeCom = tk.StringVar()
        self.TimeComEntry = tk.Entry(self, textvariable=self.TimeCom, font = font_value).grid(row = 7, column = 1, columnspan = 2, padx = 5, pady = 5)  
        self.TimeComHour_label = tk.Label(
            self, text='Hour / Day', font=font_, bg='ghost white').grid(row=7, column=4, padx=5, pady=5, sticky='w')

        #Disease label and Disease entry box
        self.DiseaseLabel = tk.Label(
            self, text="Disease : ", font=font_, bg='ghost white').grid(row=8, column=0, padx=5, pady=5, sticky='e')
        self.Disease = tk.StringVar()
        self.DiseaseEntry = tk.Entry(self, textvariable=self.Disease, font = font_value).grid(row = 8, column = 1, columnspan =2, padx = 5, pady = 5)

        #password label and password entry box
        self.passwordLabel = tk.Label(
            self, text="Password : ", font=font_, bg='ghost white').grid(row=9, column=0, padx=5, pady=5, sticky='e')
        self.password = tk.StringVar()
        self.passwordEntry = tk.Entry(self, textvariable=self.password, font = font_value).grid(row = 9, column =  1,columnspan = 2, padx = 5, pady = 5)      


        self.user = user_name
        prefix = 'data/'
        postfix = '_info.csv'

        user = ''

        with open(prefix + self.user + "/" + self.user + postfix, 'r', newline='') as file:
            reader = csv.DictReader(file)

            for row in reader:
                self.UserName.set(row['userName'])
                self.FristName.set(row['firstName'])
                self.LastName.set(row['firstName'])
                self.Age.set(row['age'])
                self.TimeSleep.set(row['timeSleep'])
                self.TimeCom.set(row['timeCom'])
                # job_info = self.Job.get()
                self.Job.set(row['job'])
                self.Disease.set(row['discease'])
                self.password.set(row['password'])

        #Regis button
        # self.RegisterButton = tk.Button(self, text="Edit", command = self.update).grid(row=10, column=0)
        self.RegisterButton = tk.Button(self, text="Edit", font=font_, bg='lemon chiffon', command=self.update).grid(row = 10, column = 1, padx = 5, pady =5)

        # btn_cancle
        # self.btn_cancle = tk.Button(self, text='Cancel', command=lambda: self.destroy()).grid(row=10, column=1, pady=5)
        self.btn_cancle = tk.Button(self, text='Cancel',font = font_, bg='lemon chiffon', command=lambda: self.destroy()).grid(row = 10, column = 2, padx = 5, pady =5)

        # self.label_success = tk.Label(self, text = "Registry")
        # self.label_success.grid(row = 8, column = 1)

    def update(self):
        prefix = "./data/"
        postfix = "_info.csv"

        userName_info = self.UserName.get()
        fName_info = self.FristName.get()
        lName_info = self.LastName.get()
        age_info = self.Age.get()
        timeSleep_info = self.TimeSleep.get()
        timeCom_info = self.TimeCom.get()
        # job_info = self.job.get()
        job_info = self.Job.get()
        disease_info = self.Disease.get()
        pass_info = self.password.get()

        with open(prefix + self.user + "/" + self.user + postfix, 'w', newline='') as file:
            fieldnames = ['userName','firstName','lastName','age','timeSleep','timeCom','job','discease','password']
            update = csv.DictWriter(file,fieldnames=fieldnames)
            update.writeheader()
            update.writerow({'userName': userName_info,
                             'firstName': fName_info,
                             'lastName': lName_info,
                             'age': age_info,
                             'timeSleep': timeSleep_info,
                             'timeCom': timeCom_info,
                             'job': job_info,
                             'discease': disease_info,
                             'password': pass_info})

        # self.label_success.configure(text = "updated.")
        # self.update_success()
        tk.messagebox.showinfo('Edit', 'Edit success')
        self.destroy()

    # def update_success(self):
    #     top = tk.Toplevel()
    #     label = tk.Label(top, text = "Update success.")
    #     label.grid()

    #     btn = tk.Button(top, text = "close", command = top.destroy())
    #     btn.pack()

