from functools import partial
import tkinter as tk
import csv
import os
from PIL import ImageTk, Image

class Register(tk.Toplevel):
        def __init__(self):

                font_ = ('Lato', 12)

                tk.Toplevel.__init__(self, bg='ghost white')
                # self.geometry('400x600')
                self.title('Register')
                # self.config(bg='DeepSkyBlue1')
                # label and text entry box
                img_icon = Image.open(os.path.join(os.getcwd(), 'static', 'icon', 'regis64_2.png'))
                photo_icon = ImageTk.PhotoImage(img_icon)
                label_icon = tk.Label(self, image=photo_icon, bg='ghost white')
                label_icon.image = photo_icon
                label_icon.grid(row = 0, column = 0, columnspan = 5, padx = 20, pady = 10)
                # label_icon.pack(pady = 10)


                # self.UserNameLabel = tk.Label(self, text="User Name",font = font_).grid(row=0, column=0)
                self.UserNameLabel = tk.Label(
                    self, text="User Name : ", font=font_, bg='ghost white').grid(row=1, column=0, padx=5, pady=5, sticky='e')

                self.UserName = tk.StringVar()
                # self.UserNameEntry = tk.Entry(self, textvariable=self.UserName,font = font_).grid(row=0, column=1)
                self.UserNameEntry = tk.Entry(self, textvariable=self.UserName,font = font_).grid(row = 1, column = 1,columnspan = 2, padx = 5, pady = 5) 
                 

                #password label and password entry box
                self.passwordLabel = tk.Label(
                    self, text="Password : ", font=font_, bg='ghost white').grid(row=2,  column=0, padx=5, pady=5, sticky='e')
                self.password = tk.StringVar()
                self.passwordEntry = tk.Entry(self, textvariable=self.password,font = font_).grid(row = 2,  column = 1, columnspan = 2, padx = 5, pady = 5)  

                #FristName label and text entry box
                # self.FristNameLabel = tk.Label(self, text="First name",font = font_).grid(row=1, column=0)
                self.FristNameLabel = tk.Label(
                    self, text="First name : ", font=font_, bg='ghost white').grid(row=3, column=0, padx=5, pady=5, sticky='e')
                self.FristName = tk.StringVar()
                # self.FristNameEntry = tk.Entry(self, textvariable=self.FristName,font = font_).grid(row=1, column=1)  
                self.FristNameEntry = tk.Entry(self, textvariable=self.FristName,font = font_).grid(row = 3,column = 1, columnspan = 2, padx = 5, pady = 5)  


                #LastName label and LastName entry box
                # self.LastNameLabel = tk.Label(self,text="Last name",font = font_).grid(row=2, column=0)  
                self.LastNameLabel = tk.Label(
                    self, text="Last name : ", font=font_, bg='ghost white').grid(row=4, column=0, padx=5, pady=5, sticky='e')
                self.LastName = tk.StringVar()
                # self.LastNameEntry = tk.Entry(self, textvariable=self.LastName,font = font_).grid(row=2, column=1)  
                self.LastNameEntry = tk.Entry(self, textvariable=self.LastName,font = font_).grid(row = 4, column = 1, columnspan = 2, padx = 5, pady = 5)


                #Age label and Age entry box
                # self.AgeLabel = tk.Label(self,text="Age",font = font_).grid(row=3, column=0)  
                # self.Age = tk.StringVar()
                # self.AgeEntry = tk.Entry(self, textvariable=self.Age,font = font_).grid(row=3, column=1)

                #Age label and Age entry box
                self.AgeLabel = tk.Label(
                    self, text="Age : ", font=font_, bg='ghost white').grid(row=5, column=0, padx=5, pady=5, sticky='e')
                self.Age = tk.StringVar()
                self.AgeEntry = tk.Entry(self, textvariable=self.Age,font = font_).grid(row = 5, column = 1, columnspan = 2, padx = 5, pady = 5)

                self.Age_y = tk.Label(self, text='year', font=font_, bg='ghost white').grid(
                    row=5, column=4, padx=5, pady=5, sticky = 'w')

                # #TimeSleep label and TimeSleep entry box
                # self.TimeSleepLabel = tk.Label(self,text="Bed time",font = font_).grid(row=4, column=0)  
                # self.TimeSleep = tk.StringVar()
                # self.TimeSleepEntry = tk.Entry(self, textvariable=self.TimeSleep,font = font_).grid(row=4, column=1)
                # self.TimeSleepHour_lebel = tk.Label(self, text = 'Hour / Day',font = font_).grid(row = 4 , column = 3, padx = 5)  

                #TimeSleep label and TimeSleep entry box
                self.TimeSleepLabel = tk.Label(
                    self, text="Bed time : ", font=font_, bg='ghost white').grid(row=6, column=0, padx=5, pady=5, sticky='e')
                self.TimeSleep = tk.StringVar()
                self.TimeSleepEntry = tk.Entry(self, textvariable=self.TimeSleep,font = font_).grid(row = 6, column = 1, columnspan = 2, padx = 5, pady = 5)
                self.TimeSleepHour_lebel = tk.Label(
                    self, text='Hour / Day', font=font_, bg='ghost white').grid(row=6, column=4, padx=5, pady=5, sticky='w')

                # #TimeCom label and TimeCom entry box
                # self.TimeComLabel = tk.Label(self,text="Computer time",font = font_).grid(row=5, column=0)  
                # self.TimeCom = tk.StringVar()
                # self.TimeComEntry = tk.Entry(self, textvariable=self.TimeCom,font = font_).grid(row=5, column=1)  
                # self.TimeComHour_label = tk.Label(self, text = 'Hour / Day',font = font_).grid(row = 5 , column = 3, padx = 5)  

                #TimeCom label and TimeCom entry box
                self.TimeComLabel = tk.Label(
                    self, text="Use Computer time : ", font=font_, bg='ghost white').grid(row=7, column=0, padx=5, pady=5, sticky='e')
                self.TimeCom = tk.StringVar()
                self.TimeComEntry = tk.Entry(self, textvariable=self.TimeCom,font = font_).grid(row = 7, column = 1, columnspan = 2, padx = 5, pady = 5)
                self.TimeComHour_label = tk.Label(
                    self, text='Hour / Day', font=font_, bg='ghost white').grid(row=7, column=4, padx=5, pady=5, sticky='w')

                # # Job
                # self.JobLabl = tk.Label(self, text = "Job",font = font_).grid(row = 8, column = 0)
                # self.Job = tk.StringVar()
                # self.JobEntry = tk.Entry(self, textvariable = self.Job,font = font_).grid(row = 8, column = 1)

                # Job
                self.JobLabl = tk.Label(
                    self, text="Job : ", font=font_, bg='ghost white').grid(row=8, column=0, padx=5, pady=5, sticky='e')
                self.Job = tk.StringVar()
                self.JobEntry = tk.Entry(self, textvariable = self.Job,font = font_).grid(row = 8, column =1, columnspan = 2, padx = 5, pady = 5)

                # #Disease label and Disease entry box
                # self.DiseaseLabel = tk.Label(self,text="Disease",font = font_).grid(row=6, column=0)  
                # self.Disease = tk.StringVar()
                # self.DiseaseEntry = tk.Entry(self, textvariable=self.Disease,font = font_).grid(row=6, column=1) 

                #Disease label and Disease entry box
                self.DiseaseLabel = tk.Label(
                    self, text="Disease : ", font=font_, bg='ghost white').grid(row=9, column=0, padx=5, pady=5, sticky='e')
                self.Disease = tk.StringVar()
                self.DiseaseEntry = tk.Entry(self, textvariable=self.Disease,font = font_).grid(row = 9, column = 1, columnspan = 2, padx = 5, pady = 5)

                # #password label and password entry box
                # self.passwordLabel = tk.Label(self,text="Password",font = font_).grid(row=7, column=0)  
                # self.password = tk.StringVar()
                # self.passwordEntry = tk.Entry(self, textvariable=self.password,font = font_).grid(row=7, column=1)   

                #Regis button
                # self.RegisterButton = tk.Button(self, text="Register",font = font_ ,command = self.register_user).grid(row=10, column=0, pady = 10)  
                self.RegisterButton = tk.Button(self, text="Register", font=font_, bg='lemon chiffon', command=self.register_user).grid(row = 10, column = 1, padx = 5, pady = 5)

                # self.label_success = tk.Label(self, text = "Registry")
                # self.label_success.grid(row = 9, column = 1)

                # button cancle
                # self.btn_cancle = tk.Button(self, text = 'Cancel',font = font_ ,command = lambda: self.destroy()).grid(row = 10, column = 1, pady = 10)
                self.btn_cancle = tk.Button(self, text = 'Cancel',font = font_ , bg='lemon chiffon',command = lambda: self.destroy()).grid(row = 10, column = 2, padx = 5, pady = 5)


        def register_user(self):
                prefix = "./data/"
                postfix = "_info.csv"
                userName_info = self.UserName.get()
                fName_info = self.FristName.get()
                lName_info = self.LastName.get()
                age_info = self.Age.get()
                timeSleep_info = self.TimeSleep.get()
                timeCom_info = self.TimeCom.get()
                job_info = self.Job.get()
                disease_info = self.Disease.get()
                pass_info = self.password.get()

                # file = open(prefix + os.mkdir + userName_info + postfix, "w")
                # file.write(userName_info + "\n")
                # file.write(fName_info + "\n")
                # file.write(lName_info + "\n")
                # file.write(age_info + "\n")
                # file.write(timeSleep_info + "\n")
                # file.write(timeCom_info + "\n")
                # file.write(disease_info + "\n")
                # file.write(pass_info + "\n")
                # file.close()
                # os.mkdir()
                if userName_info in os.listdir('./data/'):
                        print('logic is True == username in folder.')
                else:
                        os.mkdir('./data/' + userName_info)

                mid = '/' + userName_info + '/'



                with open(prefix + mid + userName_info + postfix, "w", newline='') as files:
                        fieldnames = ['userName','firstName','lastName','age','timeSleep','timeCom','job','discease','password']
                        writer = csv.DictWriter(files, fieldnames=fieldnames)
                        writer.writeheader()
                        writer.writerow({'userName': userName_info,
                                         'firstName': fName_info,
                                         'lastName': lName_info,
                                         'age': age_info,
                                         'timeSleep': timeSleep_info,
                                         'timeCom': timeCom_info,
                                         'job' : job_info,
                                         'discease': disease_info,
                                         'password': pass_info})
                # self.label_success.config(text = "Register success")
                tk.messagebox.showinfo('register', 'Registere is done.')


                # tk.Label(text="Register_success",
                #          fg="green", font=("calibri", 11)).pack()
                # self.UserNameEntry.delete(0, END)
                
#window
# tkWindow = tk.Tk()
# tkWindow.geometry('400x250')
# tkWindow.title('Register Form')
# buttonRegis = tk.Button(tkWindow, text="Login", command=Register)
# buttonRegis.pack()
# tkWindow.mainloop()
