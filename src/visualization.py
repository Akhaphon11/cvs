import numpy as np
from matplotlib.figure import Figure
from matplotlib.backend_bases import key_press_handler
from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg, NavigationToolbar2Tk)
import tkinter
from tkinter import * 
import tkinter as tk
from PIL import ImageTk, Image
import numpy as np 
import matplotlib.pyplot as plt
import csv

# from home_tk import HomeFrame
# from visualization_tk import VisualFrame
# from home_tk import HomeFrame


# root = Tk()
# root.title('test visualization.')
# root.geometry("400x200")

# def graph():
    # house_price = np.random.normal(200000, 25000, 5000)
    # plt.hist(house_price, 50)
    # plt.show()

# my_button = Button(root, text = "graph", command = graph)
# my_button.pack()

# root.mainloop()
MonthList = [
    "มกราคม",
    "กุมภาพันธ์",
    "มีนาคม",
    "เมษายน"
    "พฤษภาคม",
    "มิถุนายน",
    "กรกฎาคม",
    "สิงหาคม",
    "กันยายน",
    "ตุลาคม",
    "พฤศจิกายน",
    "ธันวาคม",
]
FONT_LARGE = ("Lato", 12)


# class Visual(tk.Frame):
#     def __init__(self, parent, controller):

#         tk.Frame.__init__(self,parent)

#         #ฺBottonBlack
#         self.Black_button = tk.Button(self, text="Home", font=FONT_LARGE, height=2,
#                                       width=15, bg='#FCFF9E', command=lambda: controller.show_frame(HomeFrame))
#         self.Black_button.place(x=30, y=590)

#         #ฺLabelMonth
#         self.label = tk.Label(self, text="เดือน:", font=FONT_LARGE)
#         self.label.place(x=750, y=480)
#         self.label = tk.Label(self, text="xxxxxxxxxxxx", font=FONT_LARGE)
#         self.label.place(x=800, y=480)

#         #dropdowmonth
#         self.variable = tk.StringVar()
#         self.variable = self.variable.set(MonthList[0])

#         self.opt = tk.OptionMenu(self.variable, *MonthList)
#         self.opt.config(width=90, font=('Helvetica', 12))
#         # self.opt.pack(x=800, y=300)

#         self.labelTest = tk.Label(text="", font=('Helvetica', 12), fg='red')
#         self.labelTest.pack(side="top")


#     def graph(self):
#         house_price = np.random.normal(200000, 25000, 5000)
#         plt.hist(house_price, 50)
#         plt.show()


# Implement the default Matplotlib key bindings.


class Visual:
    def __init__(self):
        root = tk.Tk()
        root.geometry('400x400')
        frame1 = tk.Frame(root)
        label = tk.Label(frame1, text = "Test Frame")
        total, ear = self.data()
        label_data = tk.Label(frame1, text = total)
        btn_update = tk.Button(frame1,text = "update", command = lambda: label_data.config(text = ear))

        frame1.pack()
        label.pack(side = "top")
        label_data.pack(side = 'bottom')
        btn_update.pack()
        root.mainloop()

    def data(self):
        data = []
        ear = []
        total = 0
        with open('../data/data_intTarget.csv', 'r', newline="") as file:
            read = csv.DictReader(file)
            for row in read:
                data.append((float(row['ear']), float(row['blink']), int(row['target'])))

            file.close()
        
        # loop for total and add ear data to list
        for i in data:
            total = total + i[:][1]
            ear.append(i[:][0])

        avg = np.average(ear) # avg ear

        # print(f"avg: {avg} \n  total : {total}")

        # # x_post = [i for i, _ in enumerate(data)]
        # return (x_post, data)
        return (total, avg)


# root = tkinter.Tk()
# root.wm_title("Embedding in Tk")

# fig = Figure(figsize=(5, 4), dpi=100)
# t = np.arange(0, 3, .01)
# fig.add_subplot(111).plot(t, 2 * np.sin(2 * np.pi * t))

# canvas = FigureCanvasTkAgg(fig, master=root)  # A tk.DrawingArea.
# canvas.draw()
# canvas.get_tk_widget().pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=1)

# toolbar = NavigationToolbar2Tk(canvas, root)
# toolbar.update()
# canvas.get_tk_widget().pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=1)


# def on_key_press(event):
#     print("you pressed {}".format(event.key))
#     key_press_handler(event, canvas, toolbar)


# canvas.mpl_connect("key_press_event", on_key_press)


# def _quit():
#     root.quit()     # stops mainloop
#     root.destroy()  # this is necessary on Windows to prevent
#     # Fatal Python Error: PyEval_RestoreThread: NULL tstate


# button = tkinter.Button(master=root, text="Quit", command=_quit)
# button.pack(side=tkinter.BOTTOM)

# tkinter.mainloop()


# If you put root.destroy() here, it will cause an error if the window is
# closed with the window manager.

