from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg, NavigationToolbar2Tk)
from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure
import matplotlib

# import matplotlib.pyplot.bar as bar_chart
import numpy as np
import matplotlib.pyplot as plt
# from PIL import ImageTk, Image
from src.register_tk import Register
from src.train_model_tk import TrainModel
from src.login_tk import Login
from tkinter import *
import tkinter as tk
from tkinter import ttk
import sys
import os
sys.path.append(os.getcwd())
from src.detectApp import App
import csv
from src.edit_user import Edit_user as eu
from math import ceil
import datetime
import time
from PIL import ImageTk, Image
# import unittest

FONT_LARGE = ("Lato", 12)
# FONT_LARGE = ("Lato", 10)
FONT_HEAD = ("Lato", 20)

class tkinterApp(Tk):
    def __init__(self):


        Tk.__init__(self)

        # self.canvas = tk.Canvas(self,bg = 'blue')
        # self.canvas.pack()
        # bg_image = tk.PhotoImage(file='bg_blue.jpg')
        # self.bg_label = tk.Label(self, image=bg_image)
        # self.bg_label.pack()

        self.config(bg='seashell3')
        self.withdraw()

        self.label_test = tk.Label(
            self, text="tkappLabelTest", fg='black', bg='seashell3')
        self.label_test.pack(side='top', anchor='ne')

        self.label_time = tk.Label(
            self, text="", fg='black', bg='seashell3')
        self.label_time.pack(side = 'top', anchor='ne')
        self.updateTime()


        self.container = Frame(self)
        # self.container.pack(side= "top", fill="both", expand=True)
        self.container.pack_forget()

        self.container.grid_rowconfigure(0, weight = 1)
        self.container.grid_columnconfigure(0, weight = 1)
        
        self.openLogin(self.container, self)

        self.name = ""

        # initializing frames to an empty array
        self.frames = {}
        # self.class_name = [IndexFrame, App, HomeFrame, Visual]
        self.set_session_name(self.name)
        self.class_name = [App, HomeFrame, Visual]

        for F in self.class_name:

            frame = F(self.container, self, self.name)

            self.frames[F] = frame

            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(HomeFrame)

    def show_frame(self, cont):
        if cont == 'homeframe':
            cont = HomeFrame
        if cont == 'Visual':
            cont = Visual
        new_frame = self.frames[cont]
        new_frame.tkraise()


    def set_session_name(self, name):
        self.name = name
        global user_session
        user_session = name
        self.label_test.configure(text = name)

    def get_user(self):
        global user_session
        user_session = self.name
        return self.name

    def updateTime(self):
        now = time.strftime("%H : %M : %S")
        self.label_time.config(text = now)
        self.after(1000, self.updateTime)

    def openLogin(self, root, controller):
        Login(root, controller)
    
    def show(self):
        self.container.pack(side="top", fill="both", expand=True)
        self.update()
        self.deiconify()

    def logout(self):
        # self.container.pack_forget()
        self.withdraw()
        self.openLogin(self.container, self)

    def close(self):
        self.tkraise()

class HomeFrame(tk.Frame):
    def __init__(self, parent, controller, session_name):

        tk.Frame.__init__(self, parent, bg= 'ghost white')
        self.con = controller
        # self.userName_label = tk.Label(self, text="Hello world.", font=FONT_LARGE)
        # self.userName_label.pack(anchor='ne')

        img_vid = Image.open(os.path.join(
            os.getcwd(), 'static', 'pic', 'logoEye_21.png'))
        photo_vid = ImageTk.PhotoImage(img_vid)
        vid_label = tk.Label(self, image=photo_vid,  bg='ghost white')
        vid_label.image = photo_vid
        # vid_label.place(x = 200, y = 100)
        vid_label.place(x = 250)
        
        # self.session_name = session_name
        self.controller = controller

        self.hello_label = tk.Label(
            self, text="APPLICATION FOR EYE FATIGUE NOTIFICATION IN CASE OF", font=FONT_HEAD, bg='ghost white')
        self.hello_label.place(x=145, y=370, bordermode=tk.OUTSIDE)
        # self.hello_label.pack(pady = 100, anchor = 'center')

        self.hello_label = tk.Label(
            self, text="PROLONGED USE OF COMPUTER USING IMAGE FROM WEB CAMERA", font=FONT_HEAD, bg='ghost white')
        self.hello_label.place(x=80, y=450)

        #ฺBottonstart
        self.start_button = tk.Button(self, text="Start", font=FONT_LARGE, bg='lemon chiffon',
                                      height=2, width=7, command=lambda: controller.show_frame(App))
        self.start_button.place(x=450, y=570)
        # self.start_button.pack(side="bottom", padx=10, pady=10)

        #ฺBottonDatavisualization
        self.setting = tk.Button(
            self, text="Setting", font=FONT_LARGE, bg='lemon chiffon', command=Setting)
        # self.Edit_button.place(x=800, y=10)
        self.setting.place(x=170, y=10)
        # self.setting.pack(side="right", padx=10, pady=10)

        #ฺBottonDatavisualization
        self.Datavisualization_button = tk.Button(
            self, text="Data Visualization", font=FONT_LARGE, bg='lemon chiffon', command=lambda: controller.show_frame(Visual))
        self.Datavisualization_button.place(x=20, y=10)
        # self.Datavisualization_button.pack(side='right', padx=10, pady=10)

        self.logout = tk.Button(self, text="Logout", font=FONT_LARGE, bg='lemon chiffon', command= lambda: controller.logout())
        # self.logout.pack(side = 'bottom',padx = 10, pady = 10, anchor = 'se')
        self.logout.place(x = 900, y = 10)


MonthList = ["เดือน","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม",
            "สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม",]

MonthList_en = ['Month', 'January', 'February', 'March', 'April', 'May',
                'June', 'July', 'August', 'September', 'October', 'November', 'December']

class Visual(tk.Frame):
    def __init__(self, parent, controller, session_name):

        tk.Frame.__init__(self, parent, bg='ghost white')

        # img_home = Image.open(os.path.join(os.getcwd(), 'static', 'icon', 'home32.png'))
        # photo_home = ImageTk.PhotoImage(img_home)
        # photo_home = photo_home.subsample(3, 3)
        # label = tk.Label(self, image=photo_home, bg='CadetBlue2')
        # label.image = photo_home
        # label.pack(pady = 20)
        # label.pack(side = 'left',pady = 20, padx = 40, anchor = 'nw')
        # label.place(x = 350, y = 10)

        

        #ฺBottonBlack
        self.btn_home = tk.Button(self, text="Home", font=FONT_LARGE, width = 8,height = 2, bg='lemon chiffon'
            , command = lambda: controller.show_frame(HomeFrame))
        # self.btn_home.image = photo_home
        # self.Black_button.place(x=30, y=590)
        self.btn_home.pack(padx = 50,pady = 20,side = "bottom", anchor = 'sw')

        self.btn_show = tk.Button(self, text= "Show Visualization",font = ('Lato', 12),width = 18, height = 1,bg = 'lemon chiffon', command  = self.visual)
        self.btn_show.pack(pady = 0, side="bottom")

        self.combo_month = ttk.Combobox(self,values = MonthList_en, width = 12, font = ('Lato', 15))
        self.combo_month.current(9)
        self.combo_month.pack(pady = 10)
        self.combo_month.bind('<<ComboboxSelected>>', lambda e: self.comboBoxSelected())

        self.label_value_month = tk.Label(
            self, text=self.combo_month.get(), font=('Lato', 15), bg='old lace')
        self.label_value_month.pack(pady = 10)

        self.pre_post_month = "_0" + str(MonthList_en.index(self.combo_month.get())) + "_"

        self.fig = None
        self.canvas = None
        self.toolbar = None
        # self.info = self.getInfo()
    
    def comboBoxSelected(self):
        sel_month = self.combo_month.get()
        self.label_value_month.config(text = sel_month)
        self.pre_post_month = "_0" + str(MonthList_en.index(self.combo_month.get())) + "_"

    def getInfo(self):
        user = user_session
        prefix = './data/'
        postfix = '_info.csv'

        info = {}

        with open(prefix + user + '/' + user + postfix, 'r', newline="") as file:
            reader = csv.DictReader(file)
            for row in reader:
                info = dict(row)
            file.close()
            
        return info

    def getData(self, name, month):
        prefix = './data/'
        data = []
        week = {
            1: [],2: [], 3: [], 4: [], 5:[]
        }
        file = os.listdir(prefix + name)
        for m in file:
            if month in m:
                data.append(m)

        #  add data to week
        for i in data:
            d = int(i[:2])
            d = ceil(d / 7.0)
            week[d].append(i)
            
        return week

    def weekToData(self):
        prefix = './data/' + user_session + '/' #path to data
        data = self.getData(user_session, self.pre_post_month) # get Data
        week_ = {1: [], 2: [], 3: [], 4: [], 5: []} # pre get data to week
        week_sum = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0} # sum of data

        # # loop get Data form dict
        for key, value in data.items():
        #     # loop for week
            for i in value:
                with open(prefix + str(i), 'r', newline='') as file:
                    read = csv.reader(file)
                    for row in read:
                        week_[key].append(row)
                    file.close()

        # loop for sum blink
        # if show notification use i[2]
        for key, value in week_.items():
            s = 0
            for i in value:
                s = s + int(i[1])
            week_sum[key] = s

        return week_sum
        # print(week_sum)

    def visual(self):

        # date = datetime.datetime()
        # pre_post_month = "_" + str(MonthList.index(self.combo_month.get())) + "_"
        data = self.weekToData()
        info = self.getInfo()

        num_month = data.keys()
        num_value = data.values()

        x = np.arange(len(num_month))
        width = 0.5

        # x_post, blink = self.bar_chart()

        self.fig = Figure(figsize=(10, 4), dpi=100)
        ax = self.fig.add_subplot(111)

        rect = ax.bar(x, num_value, width, label = 'Notification')

        ax.set_ylabel('Notification')
        ax.set_xlabel('week')
        ax.set_title('Blink Detection')
        ax.set_xticks(x)
        ax.set_xticklabels(num_month)
        ax.legend()
        
        def autoLabel(rects):
            for rect in rects:
                height  = rect.get_height()
                ax.annotate(f'{height}',
                xy = (rect.get_x() + rect.get_width() / 2, height),
                xytext = (0,3),
                textcoords = 'offset points',
                ha = 'center', va ='bottom')

        autoLabel(rect)

        ax.text(6, 40, 'Month : {}'.format(self.combo_month.get()))
        ax.text(6, 35, 'First name : {}'.format(info['firstName']))
        ax.text(6, 30, 'Last name : {}'.format(info['lastName']))
        ax.text(6, 25, 'Age : {} '.format(info['age']))
        ax.text(6, 20, 'Sleep Time : {} hour/day'.format(info['timeSleep']))
        ax.text(6, 15, 'Use Computer : {} hour/day' .format(info['timeCom']))
        ax.text(6, 10, 'Job : {}'.format(info['job']))
        ax.text(6, 5, 'Disease : {}'.format(info['discease']))

        if self.canvas != None:
            self.canvas._tkcanvas.destroy()
            self.toolbar.destroy()
            self.canvas = None
            self.toolbar = None

        self.fig.tight_layout()
        # if self.canvas == None:
            # bar = plt.bar(x_post, blink)
        self.canvas = FigureCanvasTkAgg(self.fig, self)
        # # canvas.show()
        self.canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)

        self.toolbar = NavigationToolbar2Tk(self.canvas, self)
        self.toolbar.update()
        self.canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        
        
class Setting(tk.Toplevel):

    def __init__(self):
        tk.Toplevel.__init__(self, bg='ghost white')
        self.geometry("450x295")
        self.title("Setting")
        # canvas = tk.Canvas(self)
        # canvas.create_line(15,15,200,15)
        # canvas.pack(expand = 1)   

        img_train = Image.open(os.path.join(os.getcwd(), 'static', 'icon', 'train128_2.png'))
        photo_train = ImageTk.PhotoImage(img_train)
        label_train = tk.Label(self, image=photo_train, bg='ghost white')
        label_train.image = photo_train
        # label_icon.pack(pady = 20)
        # label_train.pack(side = 'left',pady = 20, padx = 40, anchor = 'nw')
        label_train.place(x = 40, y = 20)


        img_edit = Image.open(os.path.join(os.getcwd(), 'static', 'icon', 'edit128_2.png'))
        photo_edit = ImageTk.PhotoImage(img_edit)
        label_edit = tk.Label(self, image=photo_edit, bg='ghost white')
        label_edit.image = photo_edit
        # label_icon.pack(pady = 20)
        # label_edit.pack(side = 'right',anchor = 'ne')
        label_edit.place(x = 270, y = 20)

        self.btn_trainModel = tk.Button(self, text="Train Model",font = ('Lato', 14),bg = 'navajo white', command=TrainModel)
        # self.btn_trainModel.pack(side = 'left',padx =  55, pady =  50, anchor='w')
        self.btn_trainModel.place(x = 45, y = 160)


        self.btn_edit = tk.Button(self, text="Edit personal user",font = ('Lato', 14),bg = 'navajo white', command = lambda: eu(user_session))
        # self.btn_edit.pack(side='left', padx=80, pady=50, anchor='w')
        self.btn_edit.place(x = 235, y = 160)

        self.btn_close = tk.Button(self, text="close",font = ('Lato', 12),bg = 'navajo white', command = lambda: self.destroy())
        # self.btn_visual.pack(side = 'bottom',padx = 10,pady =20, anchor = 'center')
        self.btn_close.place(x = 190, y = 240)

# if __name__ == "__main__":
#     unittest.main()

# if __name__ == "__main__":
#     app = tkinterApp()
#     app.geometry("1000x700")
#     app.title('computer vision demo')
#     app.mainloop()



