import tkinter as tk
from functools import partial
import tkinter.font as font
import os
import csv

from PIL import ImageTk, Image
from src.register_tk import Register

# from root_tk import HomeFrame
# from home_tk import HomeFrame

#window



class Login(tk.Toplevel):
    
        def __init__(self, parent, controller):
            tk.Toplevel.__init__(self, parent, bg='ghost white')
            self.title('test toplevel')

            self.con = controller
           
            self.login_screen(tpv = self)
        
        def login_screen(self, tpv):
            font_ = ('Lato', 12)
            # self.login_screen = tk.Toplevel(self, bg = 'LightSkyBlue1')
            self.login_screen = tpv
            self.login_screen.title("Login")
            # self.login_screen.geometry('370x350')

            img_icon = Image.open(os.path.join(os.getcwd(),'static', 'icon', 'login64_2.png'))
            photo_icon = ImageTk.PhotoImage(img_icon)
            label_icon = tk.Label(self.login_screen,
                                  image=photo_icon, bg='ghost white')
            label_icon.image = photo_icon
            label_icon.grid(row = 0, column = 0, columnspan = 3, pady = 10)
            # label_icon.place(x=10,y = 10)
            
            self.firstNameLabel = tk.Label(self.login_screen, font=font_, text="User name : ", bg='ghost white').grid(
                row=1,  column=0, padx=5, pady=5, sticky='e')
            self.firstName = tk.StringVar()
            # self.firstNameEntry = tk.Entry(self.login_screen, font = font_, textvariable=self.firstName).place(x = 150, y = 100)
            self.firstNameEntry = tk.Entry(self.login_screen, font = font_, textvariable=self.firstName).grid(row = 1, column = 1, columnspan  = 2, padx = 5, pady = 5)

            self.passwdLabel = tk.Label(self.login_screen, text="Password : ", font=font_, bg='ghost white').grid(
                row=2, column=0, padx=5, pady=5, sticky='e')
            self.passwd = tk.StringVar()
            # self.passwdEntry = tk.Entry(self.login_screen, textvariable=self.passwd,font = font_, show = "*").place(x = 350, y = 200)
            self.passwdEntry = tk.Entry(self.login_screen, textvariable=self.passwd,font = font_, show = "*").grid(row = 2, column = 1, columnspan = 2, padx = 5, pady = 5)

            #login button
            self.loginButton = tk.Button(self.login_screen, text="Login", font=font_,
                                        bg='lemon chiffon', command=self.login_verify).grid(row = 3, column = 0, padx = 5, pady = 5, sticky = 'e')
            # self.loginButton = tk.Button(self.login_screen, text="Login", font=font_, command=self.login_verify).pack()

            self.btn_cancle = tk.Button(self.login_screen, text='Cancel', font=font_, bg='lemon chiffon',
                                        command=lambda: self.cancle()).grid(row = 3,  column = 2, padx = 5, pady = 5, sticky = 'w')
            # self.btn_cancle = tk.Button(self.login_screen, text = 'Cancel',font = font_, command = lambda: self.login_screen.destroy()).pack()

            self.btn_reg = tk.Button(self.login_screen, text = 'Register', font = font_ , bg = 'lemon chiffon',
                                        command = Register).grid(row = 3, column = 1, padx = 5, pady = 5)

        def cancle(self):
            self.login_screen.destroy()
            # self.quit()
            self.con.destroy()

        def login_verify(self):
            userName = self.firstName.get()
            passwd = self.passwd.get()
            prefix = 'data/'
            postfix = '_info.csv'

            list_files = os.listdir('data/')
            if userName in list_files:
                    pass_user = ""
                    with open(prefix + userName + "/" + userName + postfix, 'r', newline='') as csvfile:
                            reader = csv.DictReader(csvfile)
                            # print("Hello : " + reader['userName'])
                            for row in reader:
                                    # print('Hello :'+row['userName'])
                                    # print('login none')
                                    pass_user = row['password']
                    if passwd == pass_user:
                            # print('login successs')
                            # global user_session
                            user_session = userName
                            # HomeFrame.label_name.config(text = user_session)
                            # print(user_session)
                            self.login_success()
                    else:
                            # print('password not correct.')
                            self.password_not_recognised()
            else:
                    # print('No user.')
                    self.user_not_found()


        def login_success(self):
            self.login_success_screen = tk.Toplevel(
                self.login_screen, bg='ghost white')
            self.login_success_screen.geometry('100x100')

            self.login_success_screen.title('Success')
            self.login_success_screen.geometry('150x100')
            label_success = tk.Label(
                self.login_success_screen, text="Login Success", bg='ghost white', font=('Lato', 12))
            label_success.pack(side = 'top', pady = 15)
            btn_success = tk.Button(self.login_success_screen, text='Ok', font=(
                'Lato', 12), bg='lemon chiffon', command=self.delete_login_success)
            btn_success.pack(side = 'top', pady = 5)

        def delete_login_success(self):
            self.login_success_screen.destroy()
            
            # self.controller.show_frame(HomeFrame)
            # self.controller.set_session_name(self.firstName)
            self.login_screen.destroy()
            self.con.set_session_name(self.firstName.get())
            self.con.show()
            # self.update()
            # self.deiconify()
            # self.controller.set_session_name(self.firstName.get())

        def password_not_recognised(self):
            self.password_not_recog_screen = tk.Toplevel(
                self.login_screen, bg='ghost white')
            self.password_not_recog_screen.title('Success')
            self.password_not_recog_screen.geometry('150x100')
            label_success = tk.Label(
                self.password_not_recog_screen, text="Invalid Password", bg='ghost white')
            label_success.pack()
            btn_pass_not_recog = tk.Button(self.password_not_recog_screen, text=" Ok",  bg='lemon chiffon', command=self.delete_password_not_recognise)
            btn_pass_not_recog.pack()

        def delete_password_not_recognise(self):
            self.password_not_recog_screen.destroy()

        def user_not_found(self):
            self.user_not_found_screen = tk.Toplevel(
                self.login_screen, bg='ghost white')
            self.user_not_found_screen.title('Success')
            self.user_not_found_screen.geometry('150x100')
            label_not_found= tk.Label(
                self.user_not_found_screen, text="user not found.", bg='ghost white')
            label_not_found.pack()
            btn_not_found = tk.Button(self.user_not_found_screen, text="Ok",
                                    bg='lemon chiffon', command=self.delete_user_not_found_screen)
            btn_not_found.pack()

        def delete_user_not_found_screen(self):
            self.user_not_found_screen.destroy()

        def get(self):
            return self.firstName

        




# tkWindow = tk.Tk()
# tkWindow.geometry('400x150')
# tkWindow.title('Login Form')
# buttonLogin = tk.Button(tkWindow, text="Login", command=Login)
# buttonLogin.pack()

# tkWindow.mainloop()

# root =  tk.Tk()
class test(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        self.title('Main frame')

        self.frame = tk.Frame(self)
        self.frame.pack_forget()

        self.withdraw()

        # self.btn = tk.Button(self.frame, text = 'open Login', command = lambda: self.openLogin(self))
        # self.btn.pack()

        self.openLogin(self.frame, self)

        self.mainloop()

    def hide(self):
        self.withdraw()

    def openLogin(self, root, controller):
        # self.hide()
        Login(root, controller)
        # self.update()
        # self.deiconify()

    def show(self):
        self.update()
        self.deiconify()

# test()


