import tkinter as tk
from tkinter import ttk
from functools import partial
from detectApp import App
from myVideoCapture import MyVideoCapture as vid


FONT_LARGE = ("Lato", 12)

class DetectFrame(tk.Frame):
    def __init__(self, parent, controller):
        # Create the window
        # self.window = parent
        # self.window.geometry("1000x700")
        # self.window.title("Detect app")
        # self.window.protocol("WM_DELETE_WINDOW", self.window.quit) # Enable the close icon
        tk.Frame.__init__(self,parent)

        
        self.detect_frame = tk.Frame(self)
        self.detect_frame.pack(side = "left")

        state = False

        self.app = App(self.detect_frame, state)

        ear, blink, pred, noti = [0,0,0,0]

        #ฺBottonStart
        self.start_button = tk.Button(self, text="เริ่ม",font=FONT_LARGE, height = 3, width = 7, bg='#B5F64A', command = self.app)
        self.start_button.place(x=425, y=500)
        # self.start_button.size(hight=100, width=100)

	    #ฺBottonStop
        self.stop_button = tk.Button(self, text="หยุด",font=FONT_LARGE , height =3, width =7, bg='#F65C4A', command = lambda: None)
        self.stop_button.place(x=500, y=500)

        #ฺBottonBlack
        self.Black_button = tk.Button(self, text="กลับสู่หน้าหลัก",font=FONT_LARGE , height =2, width =15, bg='#FCFF9E')
        self.Black_button.place(x=30, y=590)

        #ฺLabelEAR
        self.label = tk.Label(self,text = "EAR:",font=FONT_LARGE )
        self.label.place(x=750, y=75)
        self.label = tk.Label(self,text = f" {ear}",font=FONT_LARGE )
        self.label.place(x=800, y=75)

        #ฺLabelBling
        self.label = tk.Label(self,text = "Blink:",font=FONT_LARGE )
        self.label.place(x=750, y=100)
        self.label = tk.Label(self,text = f" {blink}",font=FONT_LARGE )
        self.label.place(x=800, y=100)

        #ฺLabelPredict
        self.label = tk.Label(self,text = "Predict:",font=FONT_LARGE )
        self.label.place(x=750, y=125)
        self.label = tk.Label(self,text = f" {pred}",font=FONT_LARGE )
        self.label.place(x=810, y=125)

        #ฺLabelNotification
        self.label = tk.Label(self,text = "Notification:",font=FONT_LARGE )
        self.label.place(x=750, y=150)
        self.label = tk.Label(self,text = f" {noti}",font=FONT_LARGE )
        self.label.place(x=835, y=150)

        # create frame


    # def app(self, frame, state = False):
    #     return App(frame, state)



# if __name__ == "__main__":
#     root = tk.Tk()
#     app = Appdetect_camera(root)
#     root.mainloop()
   
