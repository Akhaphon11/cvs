from win10toast import ToastNotifier as tf
import time
import os

class Notification(object):
    def __init__(self, countNoti):
        self.toaster = tf()  # call library notifier
        self.num = countNoti  # addsign number notification 1,2,3
        self.duration = 0  # duration show notify time

        self.text_1 = "Take some break to rest one's eyes."
        self.text_2 = "Take a change posture."
        self.text_3 = "Take a break for a restroom."

    def notification(self, text = "", duration = 0, icon = None):
        self.toaster.show_toast("Notification!", text, icon_path=icon, duration=duration,threaded =True)

    def getNoti(self):
        if self.num == 1:
            self.toaster.show_toast(self.text_1, icon_path=os.path.join(
                os.getcwd(), 'static', 'icon', 'eye.ico'), duration=5, threaded=True)

        elif self.num == 2:
            self.toaster.show_toast(self.text_2, icon_path=os.path.join(
                os.getcwd(), 'static', 'icon', 'posture.ico'), duration=10, threaded=True)

        elif self.num == 3:
            self.toaster.show_toast(self.text_3,  icon_path=os.path.join(
                os.getcwd(), 'static', 'icon', 'toilet.ico'), duration=20, threaded=True)
        else:
            self.toaster.show_toast("Exception something.", 5)

# n = Notification(3)
# n.getNoti()

