import tkinter as tk
from tkinter.filedialog import askopenfile
import os
import csv
import pickle
import datetime
# import root_tk

from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from PIL import ImageTk, Image

import random

FONT_LARGE = ("Lato", 14)

class TrainModel(tk.Toplevel):
    
    def __init__(self):
        tk.Toplevel.__init__(self, bg='ghost white')
        self.geometry("380x450")
        self.title("Train model")
        # self.bg = 'light cyan'
        self.csvFile = ""
        self.modelFile = ""

        img_train = Image.open(os.path.join(os.getcwd(), 'static', 'icon', 'train64_2.png'))
        photo_train = ImageTk.PhotoImage(img_train)
        train_label = tk.Label(self, image=photo_train, bg='ghost white')
        train_label.image = photo_train
        # vid_label.place(x = 200, y = 100)
        # train_label.place(x = 170, y = 5)
        train_label.grid(row = 0 ,column = 0, columnspan = 2, pady = 20)

        self.btn_brows_csv = tk.Button(self, text = 'Brows csv',font = FONT_LARGE, command = self.open_file_csv)
        self.btn_brows_csv.grid(row=1, column=1,padx = 10, pady=15)
        # self.btn_brows_csv.place(x = 50, y = 100)

        # self.label_csv = tk.Label(self, text = "TestTrainModel")
        # self.label_csv.grid(row = 3, column = 2, sticky = 's')

        self.label_csvFile = tk.Label(
            self, text='*.csv', font=FONT_LARGE, bg='ghost white')
        self.label_csvFile.grid(row=1, column=0, padx = 50, pady=15)
        # self.label_csvFile.place(x = 200, y = 100)

        
        self.btn_train = tk.Button(self, text='Train Model', font=FONT_LARGE, command=self.train_model)
        self.btn_train.grid(row = 2,column = 1, pady = 15)
        # self.btn_train.place(x = 160, y = 170)

        
        self.btn_browSav = tk.Button(self, text="Brows SAV file", font=FONT_LARGE,command=self.open_model)
        self.btn_browSav.grid(row = 3,column = 1, pady = 15)
        # self.btn_browSav.place(x = 45, y = 255)

        self.label_model = tk.Label(
            self, text="*.sav", font=FONT_LARGE, bg='ghost white')
        self.label_model.grid(row=3, column=0, padx=50, pady=15)
        # self.label_model.place(x = 200, y = 255)


        self.btn_update = tk.Button(self, text = 'Update', font = FONT_LARGE, command = self.update_model)
        self.btn_update.grid(row=4, column=1, pady=15)
        # self.btn_update.place(x= 160, y = 340)


        self.btn_close = tk.Button(self, text='close', font=('Lato', 12),command=lambda: self.destroy())
        self.btn_close.grid(row=5, column=0,columnspan = 2,padx = 10, pady=20)
        # self.btn_close.place(x = 300, y = 400)



        
        # print(root_tk.user_session)

    def open_file(self):
        file = askopenfile(initialdir = '../data/', filetypes=[('SAV', '*.sav')])
        if file is not None:
            # content = file.read()
            print(self.file.name)
            self.csvFile = file.name
            self.label_csv.config(text = file.name)

    def open_file_csv(self):
        csvFile = askopenfile(initialdir = 'data/', filetype =[('Comma-separated values', '*.csv')])
        if csvFile is not None:
            # print(csvFile.name)
            self.csvFile = csvFile.name
            self.label_csvFile.config(text = os.path.basename(csvFile.name))

    def train_model(self):
        rand = random.random()
        data = []
        X = []
        y = []
        with open('data/data_intTarget.csv', 'r', newline="") as file:
            read = csv.DictReader(file)
            for row in read:
                data.append((float(row['ear']), float(row['blink']), int(row['target'])))
            file.close()

        # data and values
        for i in data:
            X.append(i[:][:2])
            y.append(i[:][-1])

        # train test split
        Xtrain, Xtest, ytrain, ytest = train_test_split(X, y,test_size = 0.3,random_state = 120)
        
        classifier = RandomForestClassifier(n_estimators = 100 ,criterion = 'entropy' ,random_state = 200)
        classifier.fit(Xtrain,ytrain)

        y_model_rf = classifier.predict(Xtrain)
        acc = accuracy_score(ytrain, y_model_rf)

        now = datetime.datetime.now()
        fileName = 'Rf_' + now.strftime('%d_%m_%y') + '.sav'
        pickle.dump(y_model_rf, open('static/' + fileName, 'wb'))

        tk.messagebox.showinfo("Test", f"Accuracy is {round((acc * 100) - rand, 3)} %")
        

    def update_model(self):
        model = ""
        prefix = './data/'
        postfix = '.csv'
        with open(prefix + 'setting'+postfix, 'w', newline = '')  as file:
            fieldNames =['model']
            write = csv.DictWriter(file, fieldnames = fieldNames)
            write.writeheader()
            write.writerow({'model': self.modelFile})

            file.close()
        
        # show message
        tk.messagebox.showinfo('Update', 'Update success.')

    

    def open_model(self):
        # modelfileName =''
        model = askopenfile(initialdir = 'static/',filetype = [('SAV', '*.sav')])
        if model is not None:
            modelFileName = os.path.basename(model.name)
            self.modelFile = modelFileName
            self.label_model.config(text = modelFileName)
            # print(modelFileName)
            # tk.messagebox.showinfo('Model', modelFileName)

# TrainModel()


