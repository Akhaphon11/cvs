import tkinter as tk

from edit_user import Edit_user
from train_model_tk import TrainModel


class Setting(tk.Toplevel):

    def __init__(self):
        tk.Toplevel.__init__(self)
        self.geometry("100x100")

        self.btn_trainModel = tk.Button(self, text = "Train Model", command = TrainModel)
        self.btn_trainModel.pack(anchor = 'center')

        self.btn_edit = tk.Button(self, text = "Edit", command = Edit_user)
        self.btn_edit.pack(anchor='center')

        self.btn_visual = tk.Button(self, text = "Visualization.")
        self.btn_visual.pack(anchor='center')

# if __name__ == "__main__":
#     root = tk.Tk()

#     btn_test = tk.Button(root, text = "test", command  = Setting)
#     btn_test.pack()

#     root.mainloop()
