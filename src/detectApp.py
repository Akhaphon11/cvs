import cv2
import sys
import os
sys.path.append(os.getcwd())
from src.myVideoCapture import MyVideoCapture as vid

from src.detect import Detect
import tkinter as tk
import PIL.Image
import PIL.ImageTk
from src.predict import Predict
import numpy as np
# from predict import Predict
# from src.notification import Notification as nf
import tkinter as tk
import time
import csv
import datetime
from win10toast import ToastNotifier as tf
# from root_tk import Setting

# from setting_tk import *

FONT_LARGE = ("Lato", 12)

# import os
class App(tk.Frame):
    def __init__(self, parent, controller ,session_name,video_source=0):
        # tk.Frame.__init__(self, frame)
        # tk.Toplevel.__init__(self, frame)
        self.user = session_name
        tk.Frame.__init__(self, parent, bg='ghost white')
        # self.window.title(window_title)
        self.video_source = video_source

        self.vid = Detect(self.video_source)
        # self.vid = MyVideoCapture(self.video_source)

        self.toaster = tf()

        #get Total
        self.ls = []
        self.EYE_AR_THRESH = 0.23
        self.EYE_AR_CONSEC_FRAMES = 3
        self.TOTAL = 0   ########################
        self.COUNTER = 0
        self.avg = 0.0  # average EAR   #################
        self.frame = 1  # num of frame
        self.lsEAR = []  # list EAR of frame
        self.lsCounterBlink = []  # list value count blink
        # self.prediction = self.prediction()
        self.sumNoti = 0 # sum notification  ######################

        self.data = []
        self.delay = 15
        
        self.canvas = tk.Canvas(self,width= self.vid.width, height = self.vid.height)
        # self.canvas.pack(padx = 5, pady = 5,anchor = 'nw')
        self.canvas.grid(padx = 5, pady = 5, row = 0, column = 0,rowspan = 5)

        # self.btn_snapshot=tk.Button(self, text="Snapshot", width=50, command=self.snapshot)
        # self.btn_snapshot.grid(padx=5, pady=5)
        #ฺLabelEAR
        self.label_ear = tk.Label(self, text="EAR : ", font=FONT_LARGE, bg= 'ghost white')
        self.label_ear.grid(padx=5, pady=5, row=0, column=1, sticky = 'e')
        self.label_ear = tk.Label(
            self, text=f" {round(self.avg, 6)}", font=FONT_LARGE, bg='ghost white')
        # self.label_ear = tk.Label(self, text=f"0.194568", font=FONT_LARGE, bg= 'LightSkyBlue1')

        self.label_ear.grid(padx=5, pady=5, row=0, column=2, sticky = 'w')
        # self.label.pack()

        #ฺLabelBling
        self.label_blink = tk.Label(
            self, text="Blink : ", font=FONT_LARGE, bg='ghost white')
        self.label_blink.grid(padx=5, pady=5, row = 1, column = 1, sticky = 'e')
        self.label_blink = tk.Label(
            self, text=f" {self.TOTAL}", font=FONT_LARGE, bg='ghost white')
        # self.label_blink = tk.Label(self, text=f"1", font=FONT_LARGE, bg= 'LightSkyBlue1')

        self.label_blink.grid(padx=5, pady=5, row=1, column=2, sticky = 'w')

        #ฺLabelPredict
        self.label_pred = tk.Label(
            self, text="Result of predict : ", font=FONT_LARGE, bg='ghost white')
        self.label_pred.grid(padx = 5, pady = 5,row = 2, column = 1, sticky = 'e')
        self.label_pred = tk.Label(
            self, text=f" {self.prediction()[1]}", font=FONT_LARGE, bg='ghost white')
        # self.label_pred = tk.Label(self, text=f"1", font=FONT_LARGE, bg= 'LightSkyBlue1')

        self.label_pred.grid(padx=5, pady=5, row=2, column=2, sticky = 'w')

        #ฺLabelNotification
        self.label_noti = tk.Label(
            self, text="No of Notification : ", font=FONT_LARGE, bg='ghost white')
        self.label_noti.grid(padx = 5, pady = 5,row = 3, column = 1, sticky = 'e')
        self.label_noti = tk.Label(
            self, text=f" {self.sumNoti}", font=FONT_LARGE, bg='ghost white')
        # self.label_noti = tk.Label(self, text=f"3", font=FONT_LARGE, bg= 'LightSkyBlue1')

        self.label_noti.grid(padx=5, pady=5,row = 3, column = 2, sticky = 'w')

        self.label_model = tk.Label(
            self, text='Model : ', font=FONT_LARGE, bg='ghost white')
        self.label_model.grid(padx=5, pady=5, row=4, column=1, sticky = 'e')
        self.label_model = tk.Label(self, text=self.getModel()[
                                    :-4], font=FONT_LARGE, bg='ghost white')
        self.label_model.grid(padx=5, pady=5, row=4, column=2, sticky = 'w')


        self.btn_start = tk.Button(self, pady= 5, text = "Start", font = FONT_LARGE, bg= 'lemon chiffon', command = lambda: self.update())
        self.btn_start.grid(padx=5, pady=35, row=5, column=1)
        # self.btn_start.pack(side = 'left')


        self.btn_stop = tk.Button(self, pady = 5,text = "Stop",font = FONT_LARGE, bg= 'lemon chiffon', command = self.stop)
        self.btn_stop.grid(padx=5, pady=20, row = 5, column = 2)
        # self.btn_stop.pack(side = 'left')


        self.btn_home = tk.Button(self, text = 'Home', font = FONT_LARGE, bg= 'lemon chiffon', command = lambda: controller.show_frame("homeframe"))
        self.btn_home.grid(padx=5, pady=5, row = 5, column = 0)
        # self.btn_home.pack(side = 'left')

    def stop(self):
        mb_button = tk.messagebox.askyesno('exit', 'are you exit?')
        if mb_button == True:
            now = datetime.datetime.now()
            strNow = now.strftime('%d_%m_%y')
            with open(f'./data/{strNow}.csv', 'a', newline="") as file:
                write = csv.writer(file)
                # write.writerow(self.data)
                for i in self.data:
                    write.writerow(i)
                file.close()
            # self.destroy()
            self.after_cancel(after_id)
            # self.after_cancel(self)
            self.vid.stop_video()
        elif mb_button == False:
            pass

        # now = datetime.datetime.now()
        # strNow = now.strftime('%d_%m_%y')
        # with open(f'../data/{strNow}_1.csv', 'a', newline="") as file:
        #     write = csv.writer(file)
        #     # write.writerow(self.data)
        #     for i in self.data:
        #         write.writerow(i)
        #     file.close()
        # self.destroy()
        # # self.after_cancel(self)
        # self.vid.stop_video()

    # not use now
    def snapshot(self):
        # frame = self.video.get_frame()
        (ret, frame) = self.vid.get_frame_detect()
        # if frame:
        #     cv2.imwrite("frame-" + time.strftime("%d-%m-%Y-%H-%M-%S") + ".jpg", cv2.cvtColor(frame, cv2.COLOR_RGB2BGR))
        if ret:
            cv2.imwrite("frame-" + time.strftime("%d-%m-%Y-%H-%M-%S") + ".jpg", cv2.cvtColor(frame, cv2.COLOR_RGB2BGR))


    def update(self):
        ret, frame = self.vid.get_frame_detect()
        # if ret :
        #     self.photo = PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(frame))
        #     self.canvas.create_image(0, 0, image=self.photo, anchor=tk.NW)
        # else:
        #     pass

        try:
            if ret :
                self.photo = PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(frame))
                self.canvas.create_image(0, 0, image=self.photo, anchor=tk.NW)
        except Exception as e:
            # print(e)
            self.destroy()
        
        global after_id
        after_id = self.after(self.delay, self.update)
        # else:
        #     self.window.destroy()
        #     vid(0).stop_video()
        # self.after(self.delay, self.update)
        prefix = './data/'
        postfix = '.csv'
        
        # file = open(prefix + "test" + postfix, 'a', newline="0")

        self.getTOTAL(self.vid.ear)
        self.lsEAR.append(self.vid.ear)
        # logic in loop under code
        if self.frame == 180:
            self.sumNoti += 1
            self.getNoti(self.sumNoti)
            self.label_noti.config(text = f'{self.sumNoti}')
            self.label_pred.config(text=f"Eye Fatigue Detection")

        elif self.frame == 122400:
            self.sumNoti += 1
            self.getNoti(self.sumNoti)
            self.label_noti.config(text=f'{self.sumNoti}')
            self.label_pred.config(text=f"Eye Fatigue Detection")

        elif self.frame == 151200:
            self.sumNoti += 1
            self.getNoti(self.sumNoti)
            self.label_noti.config(text=f'{self.sumNoti}')
            self.label_pred.config(text=f"Eye Fatigue Detection")

        else:
            pass

        if self.sumNoti > 3 and self.frame == 800:
            self.sumNoti = 0
        
        self.ls.append(self.vid.ear)
        if (self.frame % 1800 == 0):

            self.avg = np.average(self.lsEAR)
            # print(f"num frame : {self.frame}")  # nun frame
            # print(f"avg : {self.avg}")  # test ear
            # print(f"total : {self.TOTAL}")  # Total blink 
            # print(f"len last lsEAR : {len(self.lsEAR)}") # len last EAR in list

            self.lsEAR.clear()
            self.addCounterBlink(self.TOTAL)

            # print(f"list of count EAR : {self.lsCounterBlink[-1]}")
            # print(f"pred : {self.prediction}") # call Prediction file and predict

            if self.prediction()[0] == 1 :
                # self.sumNoti += 1
                # if self.sumNoti ==1:
                #     # pass notification
                #     nf(self.sumNoti).getNoti()
                # elif self.sumNoti == 2:
                #     nf(self.sumNoti).getNoti()
                # elif self.sumNoti == 3:
                #     nf(self.sumNoti).getNoti()
                #     self.sumNoti =0
                self.getNoti(self.sumNoti)
                # print(f" Sum notification is {self.sumNoti}")
                # print("----------------------------\n")

                # save csv
                self.data.append((self.avg, self.lsCounterBlink[-1], self.prediction()[0]))
        else:

            #save csv
            pass

            
        self.frame += 1

        # if not self.vid:
        #     print("stop!!!!!!!!!!!!!!")
        #     self.window.stop()

        self.label_ear.config(text=f"{round(self.avg, 6)}")
        self.label_blink.config(text = f"{self.TOTAL}")

        if self.sumNoti > 0:
            self.label_pred.config(text='Eye Fatigue Detection')
        else:
            self.label_pred.config(text = f"{self.prediction()[1]}")

        # self.label_noti.config(text = f"{self.sumNoti}")

    def getData(self):
        return [self.lsEAR, self.TOTAL, self.prediction()[0], self.sumNoti]

    def getModel(self):
        path_file = './data/setting.csv'
        model = ''

        with open(path_file, 'r', newline='') as file:
            # fieldNames =['model']
            reader = csv.DictReader(file)
            for row in reader:
                model = row['model']
            # file.close()

        return model

    def getNoti(self, num):
        if num == 1:
            self.toaster.show_toast("Notification!", "Take some break to rest one's eyes.", icon_path=os.path.join(
                os.getcwd(), 'static', 'icon', 'eye.ico'), duration=5, threaded=True)
        elif num == 2:
            self.toaster.show_toast("Notification!", "Take a change posture.", icon_path=os.path.join(
                os.getcwd(), 'static', 'icon', 'posture.ico'), duration=10, threaded=True)
        elif num == 3:
            self.toaster.show_toast("Notification!", "Take a break for a restroom.",  icon_path=os.path.join(
                os.getcwd(), 'static', 'icon', 'toilet.ico'), duration=20, threaded=True)
        else:
            # self.toaster.show_toast('Exception.' ,dutation = 5)
            pass


        
    def getTOTAL(self, ear):
        if ear < self.EYE_AR_THRESH:
            self.COUNTER += 1
        else:
            if self.COUNTER >= self.EYE_AR_CONSEC_FRAMES:
                self.TOTAL += 1
            self.COUNTER = 0

    def addCounterBlink(self, blink):
        if(len(self.lsCounterBlink) == 0):
            self.lsCounterBlink.append(blink)
        else:
            self.lsCounterBlink.append(blink - sum(self.lsCounterBlink))

     # call Prediction class and return predict target
    def prediction(self):
        if self.lsCounterBlink == []:
            # return [0, 'waiting for detection....']
            return [0, "Normal"]
        else:
            pred = Predict(self.avg, self.lsCounterBlink[-1]).predictor()


        # pred = pred
        if pred == 0:
            return [0, "Normal"]
            # return [0, "Eye Fatigue Detection"]
        else:
            return [1, "Eye Fatigue Detection"]

        # # check in list is not null if null return 0
        # if self.lsCounterBlink == []:
        #     # return [0, self.lsCounterBlink]
        #     return [333, 'AAA']
        # else:
        #     # predictor assign avgEAR and blink for predictor
        #     # and add paramitter to prediction
        #     # 0 is normal
        #     # 1 is risk
        #     pred = Predict(self.avg, self.lsCounterBlink[-1]).predictor()
        #     # pred = pred
        #     if pred == 0:
        #         return [0 , "Normal"]
        #     else:
        #         return [1 , "Risk"]

# if __name__ == "__main__":
#     # App(tk.Tk(), "testJa.")
#     root = tk.Tk()
#     root.geometry('1000x700')
#     # frame = tk.Frame(root)

#     App(root, state=True)
    
#     root.mainloop()
    # pass

